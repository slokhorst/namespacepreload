<?php

use MediaWiki\MediaWikiServices;

class NamespacePreloadHooks {
	public static function onEditFormPreloadText( string &$text, Title &$title ) {
		if ( $text && strlen( $text ) > 0 ) return;     // do not override other extensions
		$ns = $title->getNamespace();
		if ( $ns < 0 ) return;                          // a special page or something

		$key = 'preload-namespace-' . $ns;
		$msg = wfMessage( $key );
		if ( $msg->isDisabled() ) return;
		$text = $msg->text();

		$mws = MediaWikiServices::getInstance();
		if ( $mws->getMainConfig()->get( 'NamespacePreloadDoPreSaveTransform' ) ) {
			$context = RequestContext::getMain();
			$po = ParserOptions::newCanonical( $context );
			$parser = $mws->getParser();
			$text = $parser->preSaveTransform( $text, $title, $context->getUser(), $po );
		}
	}
}
